package com.ld.yt_data_import;

import com.alibaba.fastjson.JSONObject;
import com.ld.service.excel.ExcelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class YtDataImportApplicationTests {
    @Autowired
    ExcelService excelService;

    @Test
    public void contextLoads() throws Exception {
        JSONObject uploadJson = JSONObject.parseObject("{\"needJinYin\":\"on\",\"filePath\":\"C:\\\\Users\\\\Administrator\\\\Desktop\\\\新建 Microsoft Excel 工作表.xlsx\",\"needPunish\":\"on\",\"queryArr\":\"1,7,8\",\"companyLine\":\"\",\"needLost\":\"on\"}\n");
        excelService.generateByJson(uploadJson);
        Thread.sleep(2000000);
    }

}
