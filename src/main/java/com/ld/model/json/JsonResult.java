package com.ld.model.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import java.io.Serializable;
@Data
public class JsonResult implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -7116523509935611678L;

    // 定义jackson对象
    private static final ObjectMapper MAPPER = new ObjectMapper();

    // 响应业务状态 这个 参考 import org.apache.http.HttpStatus; 接口下的状态
    private Integer code;

    // 响应消息
    private String msg;

    // 响应中的数据
    private Object data;

    private Integer count;
    public static JsonResult ok(Object data) {
        return new JsonResult(data);
    }

    public static JsonResult ok() {
        return new JsonResult(null);
    }

    public JsonResult() {

    }
    public JsonResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public JsonResult(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
    public JsonResult(Integer code, String msg, Object data, Integer count) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.count = count;
    }
    
    public JsonResult(Object data) {
        this.code = 200;
        this.msg = "OK";
        this.data = data;
    }
}
