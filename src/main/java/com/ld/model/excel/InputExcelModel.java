package com.ld.model.excel;

import java.io.Serializable;

/**
 * @ClassName InputExcelModel
 * @Description TODO
 * @Author 梁明辉
 * @Date 2019/7/4 10:23
 * @ModifyDate 2019/7/4 10:23
 * @Version 1.0
 */
public class InputExcelModel implements Serializable {
    private static final long serialVersionUID = 7308500714521094252L;
    private String rootFlePath;
    private String companyLine;
    private String queryArr;
    private String needLost;
    private String needJinYin;
    private String needPunish;

}
