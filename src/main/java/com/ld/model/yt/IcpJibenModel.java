package com.ld.model.yt;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/** (ICP_JIBEN) 企业基本表 */
@Data
public class IcpJibenModel implements Serializable {
    private static final long serialVersionUID = 6102755389657307220L;

    private Long id;
    /* 注册号 */
    private String zhucehao;
    /* 名称 */
    private String realName;
    /* 类型 */
    private String leixing;
    /* 组成形式 */
    private String zuchengxingshi;
    /* 只有2种。单位或个体 */
    private String getidanwei;
    /* 姓名和编号组合 */
    private String qiyefaren;
    /* 企业法人身份证号 */
    private String qiyefarenIdNum;
    /* 关联个人表ID */
    private Long personId;
    /* 派出企业名称 */
    private String paichuqiye;
    /* 住所 */
    private String zhusuo;
    /* 注册资本(数值)单位万元。 */
    private String zhuceziben;
    /* 注册资本币种。默认人民币。 */
    private String zczbBizhong;
    /* 成立日期 */
    private String chengliriqi;
    /* 营业期限自 */
    private String yingyeqixianzi;
    /* 营业期限至 */
    private String yingyeqixianzhi;
    /* 经营范围 */
    private String jingyingfanwei;
    /* 登记机关 */
    private String dengjijiguan;
    /* 核准日期 */
    private String hezhunriqi;
    /* 登记状态 */
    private String dengjizhuangtai;
    /* 吊销日期 */
    private String diaoxiaoriqi;
    /* 注册日期 */
    private String zhuceriqi;
    /* 成员出资总额 */
    private String chengyuanchuzizonge;
    /* 成员出资总额币种 */
    private String cyczzeBizhong;
    /* 企业登记机关所在区域 */
    private String quyu;
    /* 批次号 */
    private Long pch;
    /* 此字段已改为解析入库的时间。即解析到mysql数据库的时间 */
    private Date addTime;
    /* 最后一次更新时间 */
    private Date updateTime;
    /* 是否与网站数据库关联。0为不关联。2为关联 */
    private Long flag;
    /* 备用字段 */
    private Integer isLink;

    private Integer state;

    private Integer shielded;
    /* 系统添加时间 */
    private Date createTime;
    /* 企业法人展示类型 */
    private Integer qiyefarenType;
    /* 住所展示类型 */
    private Integer zhusuoType;
    /* 营业期限自展示类型 */
    private Integer yingyeqixianziType;
    /* 营业期限至展示类型 */
    private Integer yingyeqixianzhiType;
    /* 经营范围展示类型 */
    private Integer jingyingfanweiType;
    /* 注册资本展示类型 */
    private Integer zhucezibenType;
    /* 1表示抓取更新程序不可以更新 */
    private Integer candeal;
    /* 分组后的登记状态 */
    private String dengjizhuangtaistr;
    /* 发照日期 */
    private String fazhaoriqi;
    /* 统一社会信用代码 */
    private String xinyongdaima;
    /* 档案唯一编码 */
    private String weiyibianma;

    /* 重点领域 */
    private String majoryDoma; // 存储代码

    /* 行业类型 */
    private String companyType; // 存储代码 新行业分类
    private Integer organType; // 非企业法人类型 0-普通 1-事业单位法人登记信息项 2-社会团体法人 3-基金会法人
                               // 4-民办非企业

    private String regionDm; // 所属地域
    private String regionMc; // 所属地域名称

    private Long companyNum; // 企业数目
    private Integer archiveNumber;// 信用核查 公司存在条数

    private Integer dataExchangeState; // 处理状态0 待处理、1 已处理
    private Integer dataResultState; // 数据状态（0正确数据、1疑问数据）
    private String dataResultMessage; // 疑问信息（记录错误信息）

    private String website;// 网址
    private String phone;// 联系电话

    private String usedName;// 企业曾用名

}
