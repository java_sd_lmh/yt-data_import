package com.ld.model.yt;

import lombok.Data;

import java.util.Date;

@Data
public class YtBaseModel implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    /* 主键 */
    private Long id;
    /* 创建人ID */
    private Long createBy;
    /* 修改人ID */
    private Long modifyBy;
    /* 创建时间 */
    private Date createTime;
    /* 更新时间 */
    private Date updateTime;
    /* 状态 0正常,-1删除 */
    private Integer state;

}
