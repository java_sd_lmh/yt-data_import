package com.ld.model.yt;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 失信被执行人
 *
 * @author <a href="mailto:donggongai@126.com" target="_blank">蒋承臻</a>
 * @ClassName YtVerdictLostModel
 * @date 2017年12月16日 下午3:46:31
 */
@Data
@TableName("yt_verdict_lost")
@EqualsAndHashCode(callSuper = false)
public class YtVerdictLostModel extends YtBaseModel {
    private static final long serialVersionUID = 787301477052627615L;
    private Integer ownerType; // 法人类型 0-法人 1-自然人
    private String companyName; // 被执行人
    private String creditCode; // 统一社会信用代码
    private String legalPerson;// 法定代表人
    private Integer cardType;// 法人身份证件类型
    private String idCardNo;// 法人身份证号码
    private String verdictNumber;// 案号
    private String verdictName;// 执行法院
    private String regionMc;// 地域名称
    private String accordNum;// 执行依据文号
    private String accordName;// 作出执行依据单位
    private String verdictInfo;// 生效法律文书确定的义务
    private String verdictState;// 被执行人的履行情况
    private String verdictLostState;// 失信被执行人具体情况
    private String verdictTime;// 发布时间
    private String filingTime;// 立案时间
    private String haveFulfill;// 已履行部分
    private String notFulfill;// 未履行部分
    private Integer sort; // 排序字段
    private Integer ignoreState; // 移出状态：0或null正常 1移出
    private String sexName; // 性别 自然人不为空，法人为空
    private Integer age; // 年龄 自然人不为空 企业为0
    private String searchKeywords;
}
