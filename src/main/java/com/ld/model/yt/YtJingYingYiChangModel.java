package com.ld.model.yt;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 企业经营异常名录
 *
 * @author <a href="mailto:donggongai@126.com" target="_blank">蒋承臻</a>
 * @ClassName YtJingYingYiChangModel
 * @date 2017年12月16日 下午3:35:50
 */
@Data
@TableName("yt_jingyingyichang")
@EqualsAndHashCode(callSuper = false)
public class YtJingYingYiChangModel extends YtBaseModel {
    private static final long serialVersionUID = 787301477052627615L;
    private String companyName; // 企业名称
    private String creditCode;// 统一社会信用代码
    private String content;// 列入经营异常名录原因
    private String addTime;// 列入日期
    private String removeTime;// 移出日期
    private String addOffice;// 决定机关

    private String regNo; // 注册号
    private String legalPerson; // 法定代表人
    private String cerType; // 证件类型
    private String cerNo; // 证件号码
    private String specause; // 列入经营异常名录原因类型
    private String decideArea; // 作出决定机关所在区域代码
    private String validTime; // 有效时间
    private String sysCreateTime; // 导入文件中列入日期
    private Integer sort; // 排序
    private String searchKeywords;
}
