package com.ld.model.yt;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * 新版行政处罚对象
 *
 * @author <a href="mailto:903127441@qq.com" target="_blank">于鹏</a>
 * @ClassName CecBaseArchivesPunishNewModel
 * @date 2018年10月23日 上午9:56:07
 * admin_punish
 */
@JsonInclude(Include.NON_NULL)
@Data
@TableName("admin_punish")
@EqualsAndHashCode(callSuper = false)
public class AdminPunishModel {
    private static final long serialVersionUID = 1L;

    private Long id; // 主键

    private Integer state; // 状态 0：正常 -1：删除

    private Long createBy; // 创建人ID

    private Date createTime; // 创建时间

    private Long modifyBy; // 更新人ID

    private Date updateTime; // 更新时间

    private Integer shielded; // 屏蔽状态 0未屏蔽,1屏蔽

    private Integer type; // 新旧版信息区分类型：0-新版；1-旧版

    private Integer history; // 新版是否旧数据：0-不是；1-是。用于区分新版信息是否是之前已上传的旧版格式数据

    private String repeatedCheckCode;// 新旧版重复字段MD5加密码，用于判断新导入数据跟旧版双公示是否重复用

    private Long departId; // 部门
    private String departName;

    private String regionDm;// 关联区域

    private String uploadFileUuid; // 相关导入文件uuid

    private String checkCode;// MD5加密码，用于判断是否重复

    private Integer ownerType; // 区分法人还是自然人 档案类型 0企业 1个人

    private Integer dataExchangeState; // 处理状态0 待处理、1 已处理

    private Integer dataResultState; // 数据状态（0正确数据、1疑问数据）

    private String dataResultMessage; // 疑问信息（记录错误信息）

    private Long flag; // 保留字段，用于后期记录数据来源 默认0

    private String content;// 保留字段，用于记录后面可能会扩展的备注信息等
    /**
     * 行政相对人名称
     */
    private String cf_xdr_mc;
    /**
     * 行政相对人类别1 法人及非法人组织,2自然人,3个体工商户
     */
    private String cf_xdr_lb;
    /**
     * 行政相对人代码_1(统一社会信用代码)
     */
    private String cf_xdr_shxym;
    /**
     * 行政相对人代码_2 (工商注册号)
     */
    private String cf_xdr_gszc;
    /**
     * 行政相对人代码_3(组织机构代码)
     */
    private String cf_xdr_zzjg;
    /**
     * 行政相对人代码_4(税务登记号)
     */
    private String cf_xdr_swdj;
    /**
     * 行政相对人代码_5(事业单位证书号)
     */
    private String cf_xdr_sydw;
    /**
     * 行政相对人代码_6(社会组织登记证号)
     */
    private String cf_xdr_shzz;
    /**
     * 法定代表人
     */
    private String cf_frdb;
    /**
     * 法定代表人证件类型
     */
    private String cf_fr_zjlx;
    /**
     * 法定代表人身份证号
     */
    private String cf_fr_zjhm;
    /**
     * 证件类型
     */
    private String cf_xdr_zjlx;
    /**
     * 证件号码
     */
    private String cf_xdr_zjhm;
    /**
     * 行政处罚决定书文号
     */
    private String cf_wsh;
    /**
     * 违法行为类型
     */
    private String cf_wfxw;
    /**
     * 违法事实 填写普通、特许、认可、核准、登记或其他，如为“其他”，需注明具体类别。法人和非法人组织的登记信息，在登记过程中按相关部门有关规定执行。
     */
    private String cf_sy;
    /**
     * 处罚依据
     */
    private String cf_yj;
    /**
     * 处罚类别 填写警告、罚款、没收违法所得、 没收非法财物、责令停产停业、暂扣或者吊销许可证、暂扣或者吊销执照、行政拘留或其他，如为“其他”，需注明具体类别。
     */
    private String cf_cflb;
    /**
     * 处罚内容
     */
    private String cf_nr;
    /**
     * 罚款金额（万元）
     */
    private Double cf_nr_fk;
    /**
     * 没收违法所得、没收非法财物的金额（万元）
     */
    private Double cf_nr_wfff;
    /**
     * 暂扣或吊销证照名称及编
     */
    private String cf_nr_zkdx;
    /**
     * 处罚决定日期
     */

    private Date cf_jdrq;
    /**
     * 处罚有效期
     */

    private Date cf_yxq;
    /**
     * 公示截止期
     */

    private Date cf_gsjzq;
    /**
     * 处罚机关
     */
    private String cf_cfjg;
    /**
     * 处罚机关统一社会信用代码
     */
    private String cf_cfjgdm;
    /**
     * 数据来源单位
     */
    private String cf_sjly;
    /**
     * 数据来源单位统一社会信用代码
     */
    private String cf_sjlydm;
    /**
     * 备注
     */
    private String bz;

    private String punishType1;// 处罚类别1（旧版数据）

    private String punishType2;// 处罚类别2（旧版数据）

    private String punishName;// 处罚名称（旧版数据）

    private String regionNo;// 地方编码（旧版数据）

    private Date summaryDate;// 数据更新时间戳（旧版数据）

    // 以下字段为山东省双公示模板特有字段
    /**
     * 当前状态
     */
    private String cf_zt;
    /**
     * 处罚程序类型
     */
    private String cf_cxlx;
    /**
     * 处罚事项编码
     */
    private String cf_sxdm;
    /**
     * 处罚事项名称
     */
    private String cf_sxmc;
    // 以上字段为山东省双公示模板特有字段

    private Long companyId;// 企业id

    public void setCf_xdr_mc(String cf_xdr_mc) {
        this.cf_xdr_mc = cf_xdr_mc.trim();
    }

    public void setCf_xdr_lb(String cf_xdr_lb) {
        this.cf_xdr_lb = cf_xdr_lb.trim();
    }

    public void setCf_xdr_shxym(String cf_xdr_shxym) {
        this.cf_xdr_shxym = cf_xdr_shxym.trim();
    }

    public void setCf_xdr_gszc(String cf_xdr_gszc) {
        this.cf_xdr_gszc = cf_xdr_gszc.trim();
    }

    public void setCf_xdr_zzjg(String cf_xdr_zzjg) {
        this.cf_xdr_zzjg = cf_xdr_zzjg.trim();
    }

    public void setCf_xdr_swdj(String cf_xdr_swdj) {
        this.cf_xdr_swdj = cf_xdr_swdj.trim();
    }

    public void setCf_xdr_sydw(String cf_xdr_sydw) {
        this.cf_xdr_sydw = cf_xdr_sydw.trim();
    }

    public void setCf_xdr_shzz(String cf_xdr_shzz) {
        this.cf_xdr_shzz = cf_xdr_shzz.trim();
    }

    public void setCf_frdb(String cf_frdb) {
        this.cf_frdb = cf_frdb.trim();
    }

    public void setCf_fr_zjhm(String cf_fr_zjhm) {
        this.cf_fr_zjhm = cf_fr_zjhm.trim();
    }

    public void setCf_fr_zjlx(String cf_fr_zjlx) {
        this.cf_fr_zjlx = cf_fr_zjlx.trim();
    }

    public void setCf_xdr_zjlx(String cf_xdr_zjlx) {
        this.cf_xdr_zjlx = cf_xdr_zjlx.trim();
    }

    public void setCf_xdr_zjhm(String cf_xdr_zjhm) {
        this.cf_xdr_zjhm = cf_xdr_zjhm.trim();
    }

    public void setCf_wsh(String cf_wsh) {
        this.cf_wsh = cf_wsh.trim();
    }

    public void setCf_wfxw(String cf_wfxw) {
        this.cf_wfxw = cf_wfxw.trim();
    }

    public void setCf_sy(String cf_sy) {
        this.cf_sy = cf_sy.trim();
    }

    public void setCf_yj(String cf_yj) {
        this.cf_yj = cf_yj.trim();
    }

    public void setCf_cflb(String cf_cflb) {
        this.cf_cflb = cf_cflb.trim();
    }

    public void setCf_nr(String cf_nr) {
        this.cf_nr = cf_nr.trim();
    }

    public void setCf_nr_zkdx(String cf_nr_zkdx) {
        this.cf_nr_zkdx = cf_nr_zkdx.trim();
    }

    public void setCf_cfjg(String cf_cfjg) {
        this.cf_cfjg = cf_cfjg.trim();
    }

    public void setCf_cfjgdm(String cf_cfjgdm) {
        this.cf_cfjgdm = cf_cfjgdm.trim();
    }

    public void setCf_sjly(String cf_sjly) {
        this.cf_sjly = cf_sjly.trim();
    }

    public void setCf_sjlydm(String cf_sjlydm) {
        this.cf_sjlydm = cf_sjlydm.trim();
    }

    public void setBz(String bz) {
        this.bz = bz.trim();
    }

    @Override
    public String toString() {
        if (StringUtils.isNotBlank(regionDm)) {
            regionDm = regionDm;
        } else {
            regionDm = "";
        }
        if (StringUtils.isNotBlank(uploadFileUuid)) {
            uploadFileUuid = uploadFileUuid;
        } else {
            uploadFileUuid = "";
        }
        if (StringUtils.isNotBlank(checkCode)) {
            checkCode = checkCode;
        } else {
            checkCode = "";
        }
        if (StringUtils.isNotBlank(dataResultMessage)) {
            dataResultMessage = dataResultMessage;
        } else {
            dataResultMessage = "";
        }
        if (StringUtils.isNotBlank(content)) {
            content = content;
        } else {
            content = "";
        }
        if (StringUtils.isNotBlank(cf_xdr_mc)) {
            cf_xdr_mc = cf_xdr_mc;
        } else {
            cf_xdr_mc = "";
        }
        if (StringUtils.isNotBlank(cf_xdr_lb)) {
            cf_xdr_lb = cf_xdr_lb;
        } else {
            cf_xdr_lb = "";
        }
        if (StringUtils.isNotBlank(cf_xdr_shxym)) {
            cf_xdr_shxym = cf_xdr_shxym;
        } else {
            cf_xdr_shxym = "";
        }
        if (StringUtils.isNotBlank(cf_xdr_gszc)) {
            cf_xdr_gszc = cf_xdr_gszc;
        } else {
            cf_xdr_gszc = "";
        }
        if (StringUtils.isNotBlank(cf_xdr_zzjg)) {
            cf_xdr_zzjg = cf_xdr_zzjg;
        } else {
            cf_xdr_zzjg = "";
        }
        if (StringUtils.isNotBlank(cf_xdr_swdj)) {
            cf_xdr_swdj = cf_xdr_swdj;
        } else {
            cf_xdr_swdj = "";
        }
        if (StringUtils.isNotBlank(cf_xdr_sydw)) {
            cf_xdr_sydw = cf_xdr_sydw;
        } else {
            cf_xdr_sydw = "";
        }
        if (StringUtils.isNotBlank(cf_xdr_shzz)) {
            cf_xdr_shzz = cf_xdr_shzz;
        } else {
            cf_xdr_shzz = "";
        }
        if (StringUtils.isNotBlank(cf_frdb)) {
            cf_frdb = cf_frdb;
        } else {
            cf_frdb = "";
        }
        if (StringUtils.isNotBlank(cf_fr_zjhm)) {
            cf_fr_zjhm = cf_fr_zjhm;
        } else {
            cf_fr_zjhm = "";
        }
        if (StringUtils.isNotBlank(cf_fr_zjlx)) {
            cf_fr_zjlx = cf_fr_zjlx;
        } else {
            cf_fr_zjlx = "";
        }
        if (StringUtils.isNotBlank(cf_xdr_zjlx)) {
            cf_xdr_zjlx = cf_xdr_zjlx;
        } else {
            cf_xdr_zjlx = "";
        }
        if (StringUtils.isNotBlank(cf_xdr_zjhm)) {
            cf_xdr_zjhm = cf_xdr_zjhm;
        } else {
            cf_xdr_zjhm = "";
        }
        if (StringUtils.isNotBlank(cf_wsh)) {
            cf_wsh = cf_wsh;
        } else {
            cf_wsh = "";
        }
        if (StringUtils.isNotBlank(cf_wfxw)) {
            cf_wfxw = cf_wfxw;
        } else {
            cf_wfxw = "";
        }
        if (StringUtils.isNotBlank(cf_sy)) {
            cf_sy = cf_sy;
        } else {
            cf_sy = "";
        }
        if (StringUtils.isNotBlank(cf_yj)) {
            cf_yj = cf_yj;
        } else {
            cf_yj = "";
        }
        if (StringUtils.isNotBlank(cf_cflb)) {
            cf_cflb = cf_cflb;
        } else {
            cf_cflb = "";
        }
        if (StringUtils.isNotBlank(cf_nr)) {
            cf_nr = cf_nr;
        } else {
            cf_nr = "";
        }
        if (StringUtils.isNotBlank(cf_nr_zkdx)) {
            cf_nr_zkdx = cf_nr_zkdx;
        } else {
            cf_nr_zkdx = "";
        }
        if (StringUtils.isNotBlank(cf_cfjg)) {
            cf_cfjg = cf_cfjg;
        } else {
            cf_cfjg = "";
        }
        if (StringUtils.isNotBlank(cf_cfjgdm)) {
            cf_cfjgdm = cf_cfjgdm;
        } else {
            cf_cfjgdm = "";
        }
        if (StringUtils.isNotBlank(cf_sjly)) {
            cf_sjly = cf_sjly;
        } else {
            cf_sjly = "";
        }
        if (StringUtils.isNotBlank(cf_sjlydm)) {
            cf_sjlydm = cf_sjlydm;
        } else {
            cf_sjlydm = "";
        }
        if (StringUtils.isNotBlank(bz)) {
            bz = bz;
        } else {
            bz = "";
        }
        if (StringUtils.isNotBlank(cf_zt)) {
            cf_zt = cf_zt;
        } else {
            cf_zt = "";
        }
        if (StringUtils.isNotBlank(cf_cxlx)) {
            cf_cxlx = cf_cxlx;
        } else {
            cf_cxlx = "";
        }
        if (StringUtils.isNotBlank(cf_sxdm)) {
            cf_sxdm = cf_sxdm;
        } else {
            cf_sxdm = "";
        }
        if (StringUtils.isNotBlank(cf_sxmc)) {
            cf_sxmc = cf_sxmc;
        } else {
            cf_sxmc = "";
        }

        return "AdminPunishModel{" + "cf_xdr_mc='" + cf_xdr_mc + '\'' + ", cf_xdr_lb='" + cf_xdr_lb + '\'' + ", cf_xdr_shxym='"
                + cf_xdr_shxym + '\'' + ", cf_xdr_gszc='" + cf_xdr_gszc + '\'' + ", cf_xdr_zzjg='" + cf_xdr_zzjg + '\'' + ", cf_xdr_swdj='"
                + cf_xdr_swdj + '\'' + ", cf_xdr_sydw='" + cf_xdr_sydw + '\'' + ", cf_xdr_shzz='" + cf_xdr_shzz + '\'' + ", cf_frdb='"
                + cf_frdb + '\'' + ", cf_fr_zjhm='" + cf_fr_zjhm + '\'' + ", cf_xdr_zjlx='" + cf_xdr_zjlx + '\'' + ",cf_xdr_zjlx="
                + cf_xdr_zjlx + ", cf_xdr_zjhm='" + cf_xdr_zjhm + '\'' + ", cf_wsh='" + cf_wsh + '\'' + ", cf_wfxw='" + cf_wfxw + '\''
                + ", cf_sy='" + cf_sy + '\'' + ", cf_yj='" + cf_yj + '\'' + ", cf_cflb='" + cf_cflb + '\'' + ", cf_nr='" + cf_nr + '\''
                + ", cf_nr_fk=" + cf_nr_fk + ", cf_nr_wfff=" + cf_nr_wfff + ", cf_nr_zkdx='" + cf_nr_zkdx + '\'' + ", cf_jdrq=" + cf_jdrq
                + ", cf_yxq=" + cf_yxq + ", cf_gsjzq=" + cf_gsjzq + ", cf_cfjg='" + cf_cfjg + '\'' + ", cf_cfjgdm='" + cf_cfjgdm + '\''
                + ", cf_sjly='" + cf_sjly + '\'' + ", cf_sjlydm='" + cf_sjlydm + '\'' + ", bz='" + bz + '\'' + '}';
    }
}
