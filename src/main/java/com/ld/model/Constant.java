package com.ld.model;

/**
 * @ClassName Constant
 * @Description 常量类
 * @Author 梁明辉
 * @Date 2019/7/4 16:26
 * @ModifyDate 2019/7/4 16:26
 * @Version 1.0
 */
public class Constant {
    public static final int STATE_NORMAL = 0;
}
