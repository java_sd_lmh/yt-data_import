package com.ld.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @ClassName ExcelUtil
 * @Description excel工具类
 * @Author 梁明辉
 * @Date 2019/6/25 9:37
 * @ModifyDate 2019/6/25 9:37
 * @Version 1.0
 */
@Slf4j
public class ExcelUtil {
    private static final String HSSF_TYPE = ".xlx";
    private static final String XSSF_TYPE = ".xlsx";

    /**
     * @Description
     * @Author 梁明辉
     * @Date 17:17 2019-06-26
     * @ModifyDate 17:17 2019-06-26
     * @Params
     * @Return
     */
    public static Object getSheet(String filePath, int index) {
        String type = filePath.substring(filePath.lastIndexOf("."));
        try {
            Workbook workbook;
            if (HSSF_TYPE.equals(type)) {
                workbook = new HSSFWorkbook(new FileInputStream(filePath));
                return workbook.getSheetAt(index);
            } else if (XSSF_TYPE.equals(type)) {
                workbook = new XSSFWorkbook(new FileInputStream(filePath));
                return workbook.getSheetAt(index);
            } else {
                return null;
            }
        } catch (Exception e) {
            log.warn(e.getMessage());
            return null;
        }
    }

    /**
     * @Description 读取hssf数据
     * @Author 梁明辉
     * @Date 17:17 2019-06-26
     * @ModifyDate 17:17 2019-06-26
     * @Params [sheet, rowIndex, colIndex]
     * @Return java.lang.String
     */
    public static String readHSSFExcelValue(Sheet sheet, int rowIndex, int colIndex) {
        if (sheet != null) {
            Row row = sheet.getRow(rowIndex);
            if (row != null) {
                Cell cell = row.getCell(colIndex);
                if (cell != null) {
                    return getCellValue(cell);
                } else {
                    return "";
                }
            }
            return "";
        } else {
            return "";
        }
    }

    /**
     * @Description 写入数据至xlsx
     * @Author 梁明辉
     * @Date 14:31 2019-06-26
     * @ModifyDate 14:31 2019-06-26
     * @Params
     * @Return
     */
    public static void writeDataToXSSF(XSSFSheet sheet, int rowIndex, int colIndex, Object cellValue) {
        XSSFRow row;
        if (sheet.getRow(rowIndex) == null) {
            row = sheet.createRow(rowIndex);
        } else {
            row = sheet.getRow(rowIndex);
        }
        XSSFCell cell = row.createCell(colIndex);
        //为空时设置空值
        if (cellValue == null) {
            cell.setCellValue("");
        } else {
            cell.setCellValue((String) cellValue);
        }

    }

    private static String getCellValue(Cell cell) {
        String cellValue = "";
        if (cell != null) {
            int cellType = cell.getCellType();
            // 数字类型
            if (Cell.CELL_TYPE_NUMERIC == cellType) {
                // 日期类型
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                    Date date = cell.getDateCellValue();
                    cellValue = sdf.format(date);
                } else {
                    // 普通数字类型
                    DecimalFormat decimalFormat = new DecimalFormat("0");
                    cellValue = decimalFormat.format(cell.getNumericCellValue());
                    cellValue = cellValue.endsWith(".0") ? cellValue.substring(0, cellValue.indexOf(".0")) : cellValue;
                }
            } else if (Cell.CELL_TYPE_STRING == cellType) {
                // 字符串
                cellValue = cell.getStringCellValue();
            } else if (Cell.CELL_TYPE_BOOLEAN == cellType) {
                // 布尔
                cellValue = cell.getBooleanCellValue() + "";
            } else if (Cell.CELL_TYPE_FORMULA == cellType) {
                // 公式
                cellValue = cell.getCellFormula() + "";
            } else if (Cell.CELL_TYPE_BLANK == cellType) {
                // 空值

            } else if (Cell.CELL_TYPE_ERROR == cellType) {
                // 故障

            } else {// 未知类型

            }
        }
        if (StringUtils.isBlank(cellValue)) {
            return null;
        } else {
            return cellValue.trim();
        }
    }
}
