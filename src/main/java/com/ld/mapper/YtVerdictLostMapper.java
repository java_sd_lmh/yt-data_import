package com.ld.mapper;

import com.ld.model.Constant;
import com.ld.model.yt.YtVerdictLostModel;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName YtVerdictLostMapper
 * @Description TODO
 * @Author 梁明辉
 * @Date 2019/7/5 9:06
 * @ModifyDate 2019/7/5 9:06
 * @Version 1.0
 */
@Component
public interface YtVerdictLostMapper {
    /**
     * 获取失信被执行人列表,查询企业名没有()（）的情况
     *
     * @param companyName 企业名称
     * @return java.util.List<com.ld.model.yt.YtVerdictLostModel>
     * @Author 梁明辉
     * @Date 09:10 2019-07-05
     * @ModifyDate 09:10 2019-07-05
     */
    @Select("<script>" +
            "select companyName,VERDICTLOSTSTATE,filingTime from yt_verdict_lost where 1=1 and state = 0 " +
            "and companyName = #{companyName}  " +
            "</script>")
    List<YtVerdictLostModel> getLoseCreditListEq(String companyName);

    /**
     * 获取失信被执行人列表,查询企业名没有()（）的情况
     *
     * @param conSqlStr 企业名称中英文括号都有
     * @return java.util.List<com.ld.model.yt.YtVerdictLostModel>
     * @Author 梁明辉
     * @Date 09:10 2019-07-05
     * @ModifyDate 09:10 2019-07-05
     */
    @Select("<script>" +
            "select companyName,VERDICTLOSTSTATE,filingTime from yt_verdict_lost where 1=1 and state = 0 " +
            "and companyName in (#{conSqlStr})" +
            "</script>")
    List<YtVerdictLostModel> getLoseCreditListIn(String conSqlStr);

    @Select("select creditCode from yt_verdict_lost where 1=1 and companyName = #{companyName} limit 1")
    YtVerdictLostModel getOne(String companyName);
}
