package com.ld.mapper;

import com.ld.model.yt.IcpJibenModel;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

/**
 * @ClassName YtIcpJibenMapper
 * @Description TODO
 * @Author 梁明辉
 * @Date 2019/7/5 14:55
 * @ModifyDate 2019/7/5 14:55
 * @Version 1.0
 */
@Component
public interface YtIcpJibenMapper {
    @Select("select xinyongdaima,zhucehao from icp_jiben where 1=1 and realName = #{companyName} limit 1 ")
    IcpJibenModel getOne(String companyName);
}
