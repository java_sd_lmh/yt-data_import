package com.ld.mapper;

import com.ld.model.yt.AdminPunishModel;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @ClassName YtAdminPunishMapper
 * @Description TODO
 * @Author 梁明辉
 * @Date 2019/7/5 15:01
 * @ModifyDate 2019/7/5 15:01
 * @Version 1.0
 */
@Component
public interface YtAdminPunishMapper {
    /**
     * 获取行政处罚列表,查询企业名没有()（）的情况
     *
     * @param cf_xdr_mc 企业名称
     * @return java.util.List<com.ld.model.yt.YtVerdictLostModel>
     * @Author 梁明辉
     * @Date 09:10 2019-07-05
     * @ModifyDate 09:10 2019-07-05
     */
    @Select("<script>" +
            "select CF_XDR_MC,CF_SY,CF_JDRQ from admin_punish where 1=1 and STATE = 0 and SHIELDED = 0 " +
            "and CF_XDR_MC = #{cf_xdr_mc} and (CF_GSJZQ is null OR CF_GSJZQ>=STR_TO_DATE('2019-07-05','%Y-%m-%d')) " +
            "</script>")
    @Results(id = "admin_punish_map", value = {
            @Result(property = "cf_xdr_mc", column = "CF_XDR_MC"),
            @Result(property = "cf_sy", column = "CF_SY"),
            @Result(property = "cf_jdrq", column = "CF_JDRQ"),
            @Result(property = "cf_gsjzq", column = "CF_GSJZQ"),
            @Result(property = "state", column = "STATE"),
            @Result(property = "shielded", column = "SHIELDED"),
            @Result(property = "cf_xdr_shxym", column = "CF_XDR_SHXYM"),
            @Result(property = "cf_xdr_gszc", column = "CF_XDR_GSZC"),
            @Result(property = "cf_xdr_zzjg", column = "CF_XDR_ZZJG"),
            @Result(property = "cf_xdr_swdj", column = "CF_XDR_SWDJ"),
            @Result(property = "cf_xdr_sydw", column = "CF_XDR_SYDW"),
            @Result(property = "cf_xdr_shzz", column = "CF_XDR_SHZZ"),
    })
    List<AdminPunishModel> getPunishListEq(String cf_xdr_mc);

    /**
     * 获取行政处罚列表,查询企业名没有()（）的情况
     *
     * @param conSqlStr 企业名称中英文括号都有
     * @return
     * @Author 梁明辉
     * @Date 09:10 2019-07-05
     * @ModifyDate 09:10 2019-07-05
     */
    @Select("<script>" +
            "select CF_XDR_MC,CF_SY,CF_JDRQ from admin_punish where 1=1 and STATE = 0 and SHIELDED = 0 " +
            "and CF_XDR_MC in (#{conSqlStr}) and (CF_GSJZQ is null OR CF_GSJZQ>=STR_TO_DATE('2019-07-05','%Y-%m-%d')) " +
            "</script>")
    @ResultMap("admin_punish_map")
    List<AdminPunishModel> getPunishListIn(String conSqlStr);

    @Select("select CF_XDR_SHXYM,CF_XDR_GSZC,CF_XDR_ZZJG,CF_XDR_SWDJ,CF_XDR_SYDW,CF_XDR_SHZZ" +
            " from admin_punish where 1=1 and CF_XDR_MC = #{companyName} and CF_XDR_SHXYM !='00000000000000000X' limit 1")
    AdminPunishModel getOne(String companyName);
}
