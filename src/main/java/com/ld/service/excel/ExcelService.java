package com.ld.service.excel;

import com.alibaba.fastjson.JSONObject;

/**
 * @ClassName ExcelService
 * @Description excel服务接口类
 * @Author 梁明辉
 * @Date 2019/7/4 11:32
 * @ModifyDate 2019/7/4 11:32
 * @Version 1.0
 */
public interface ExcelService {
    /**
     * 根据json生成excel文件
     *
     * @param uploadJson
     */
    void generateByJson(JSONObject uploadJson);
}
