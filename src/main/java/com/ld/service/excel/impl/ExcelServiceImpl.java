package com.ld.service.excel.impl;

import com.alibaba.fastjson.JSONObject;
import com.ld.mapper.YtAdminPunishMapper;
import com.ld.mapper.YtIcpJibenMapper;
import com.ld.mapper.YtJinYinMapper;
import com.ld.mapper.YtVerdictLostMapper;
import com.ld.model.Constant;
import com.ld.model.yt.AdminPunishModel;
import com.ld.model.yt.IcpJibenModel;
import com.ld.model.yt.YtJingYingYiChangModel;
import com.ld.model.yt.YtVerdictLostModel;
import com.ld.service.excel.ExcelService;
import com.ld.util.ExcelUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @ClassName ExcelServiceImpl
 * @Description 实现类
 * @Author 梁明辉
 * @Date 2019/7/4 11:32
 * @ModifyDate 2019/7/4 11:32
 * @Version 1.0
 */
@Service
@Slf4j
@Data
public class ExcelServiceImpl implements ExcelService {
    @Value("${yt.out.file-path}")
    private String outFilePath;
    int rowIndex = 0;
    int notHave = 0;
    int have = 0;
    @Autowired
    YtVerdictLostMapper lostMapper;
    @Autowired
    YtIcpJibenMapper icpJibenMapper;
    @Autowired
    YtAdminPunishMapper punishMapper;
    @Autowired
    YtJinYinMapper jinYinMapper;

    @Async
    @Override
    public void generateByJson(JSONObject uploadJson) {
        String filePath = (String) uploadJson.get("filePath");
        if (!isEffectiveFile(filePath)) {
            return;
        }
        log.info("文件所在路径为:" + filePath);
        //获取企业名所在行，为空时默认为第二行
        int companyLine = StringUtils.isBlank((String) uploadJson.get("companyLine")) ? 1 : Integer.parseInt((String) uploadJson.get("companyLine"));
        log.info("企业名所在行数为:" + companyLine);
        //获取需要导出的行数，为空时默认是企业名所在行
        String[] queryArr = ((String) uploadJson.get("queryArr")).split(",");
        if (queryArr.length == 1 && StringUtils.isBlank(queryArr[0])) {
            queryArr[0] = "2";
        }
        //是否导出失信被执行人、经营异常、行政处罚
        boolean isNeedLost = "on".equals(uploadJson.get("needLost")) ? true : false;
        log.info("需要导出失信被执行人为:" + isNeedLost);
        boolean isNeedJinYin = "on".equals(uploadJson.get("needJinYin")) ? true : false;
        log.info("需要导出经营异常为:" + isNeedJinYin);
        boolean isNeedPunish = "on".equals(uploadJson.get("needPunish")) ? true : false;
        log.info("需要导出行政处罚为:" + isNeedPunish);
        //执行导出
        doGenerateExcel(filePath, companyLine, queryArr, isNeedLost, isNeedJinYin, isNeedPunish);

    }

    /**
     * @Description 执行导出
     * @Author 梁明辉
     * @Date 14:25 2019-07-04
     * @ModifyDate 14:25 2019-07-04
     * @Params [filePath, companyLine, queryArr, isNeedLost, isNeedJinYin, isNeedPunish]
     * @Return void
     */
    private void doGenerateExcel(String filePath, int companyLine, String[] queryArr, boolean isNeedLost, boolean isNeedJinYin, boolean isNeedPunish) {
        try {
            //string数组转为int数组
            int[] outArrLine = new int[queryArr.length];
            for (int i = 0; i < queryArr.length; i++) {
                outArrLine[i] = Integer.parseInt(queryArr[i]);
            }
            Workbook workbook = getWorkbookByPath(filePath);
            //默认取第一个
            Sheet sheet = workbook.getSheetAt(0);
            int lastRowNum = sheet.getLastRowNum();
            if (lastRowNum == 0 || lastRowNum == 1) {
                log.info("终止导出,获取到的excel为0行或者1行");
                return;
            }
            XSSFWorkbook outWorkBook = new XSSFWorkbook();
            //创建工作表
            XSSFSheet outSheet = outWorkBook.createSheet();
            //复制第一行
            Row rolFirst = sheet.getRow(0);
            for (int i = 0; i <= rolFirst.getPhysicalNumberOfCells(); i++) {
                ExcelUtil.writeDataToXSSF(outSheet, 0, i, ExcelUtil.readHSSFExcelValue(sheet, 0, i));
            }
            setRowIndex(getRowIndex() + 1);
            //生成剩余行
            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                boolean haveLose = false;
                boolean haveJinYin = false;
                boolean havePunish = false;
                String companyName = ExcelUtil.readHSSFExcelValue(sheet, i, companyLine);
                if (isNeedLost) {
                    haveLose = writeLoseCreditList(companyName, sheet, i, outSheet, outArrLine);
                }
                if (isNeedJinYin) {
                    haveJinYin = writeJinYinYiChangList(companyName, sheet, i, outSheet, outArrLine);
                }
                if (isNeedPunish) {
                    havePunish = writePunishList(companyName, sheet, i, outSheet, outArrLine);
                }
                if (!haveLose && !haveJinYin && !havePunish) {
                    log.info(companyName + "——没有查询到信息");
                }
            }
            File file = new File(outFilePath + System.currentTimeMillis() + ".xlsx");
            log.info("生成的文件名为:" + file.getName());
            //将Excel文件写入创建的file当中
            try (OutputStream stream = new FileOutputStream(file)) {
                outWorkBook.write(stream);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @Description 写出失信被执行人, 返回是否读取到数据
     * @Author 梁明辉
     * @Date 16:59 2019-07-04
     * @ModifyDate 16:59 2019-07-04
     * @Params []
     * @Return
     */
    private boolean writeLoseCreditList(String companyName, Sheet sheet, int rowIndex, XSSFSheet outSheet, int[] outArrLine) {
        String conSqlStr = getConSqlStrByName(companyName);
        List<YtVerdictLostModel> loseCreditList;
        //如果需要查询中英文括号
        if (StringUtils.isNotBlank(conSqlStr)) {
            loseCreditList = lostMapper.getLoseCreditListIn(conSqlStr);
        } else {
            loseCreditList = lostMapper.getLoseCreditListEq(companyName);
        }
        //遍历写出数据
        loseCreditList.forEach(loseModel -> {
            writeOneRow(outSheet, sheet, loseModel.getCompanyName(), "失信被执行人", loseModel.getVerdictLostState(), getIcpCode(loseModel.getCompanyName()), loseModel.getFilingTime(), rowIndex, outArrLine);
        });
        return loseCreditList.size() > 0;
    }

    /**
     * @Description 写出经营异常, 返回是否读取到数据
     * @Author 梁明辉
     * @Date 16:59 2019-07-04
     * @ModifyDate 16:59 2019-07-04
     * @Params []
     * @Return
     */
    private boolean writeJinYinYiChangList(String companyName, Sheet sheet, int rowIndex, XSSFSheet outSheet, int[] outArrLine) {
        String conSqlStr = getConSqlStrByName(companyName);
        List<YtJingYingYiChangModel> jinYinList;
        //如果需要查询中英文括号
        if (StringUtils.isNotBlank(conSqlStr)) {
            jinYinList = jinYinMapper.getJinYinListIn(conSqlStr);
        } else {
            jinYinList = jinYinMapper.getJinYinListEq(companyName);
        }
        //遍历写出数据
        jinYinList.forEach(jinYinModel -> {
            writeOneRow(outSheet, sheet, jinYinModel.getCompanyName(), "经营异常", jinYinModel.getContent(), getIcpCode(jinYinModel.getCompanyName()), jinYinModel.getAddTime(), rowIndex, outArrLine);
        });
        return jinYinList.size() > 0;
    }

    /**
     * @Description 写出行政处罚, 返回是否读取到数据
     * @Author 梁明辉
     * @Date 16:59 2019-07-04
     * @ModifyDate 16:59 2019-07-04
     * @Params []
     * @Return
     */
    private boolean writePunishList(String companyName, Sheet sheet, int rowIndex, XSSFSheet outSheet, int[] outArrLine) {
        String conSqlStr = getConSqlStrByName(companyName);
        List<AdminPunishModel> punishList;
        //如果需要查询中英文括号
        if (StringUtils.isNotBlank(conSqlStr)) {
            punishList = punishMapper.getPunishListIn(conSqlStr);
        } else {
            punishList = punishMapper.getPunishListEq(companyName);
        }
        //遍历写出数据
        punishList.forEach(punishModel -> {
            writeOneRow(outSheet, sheet, punishModel.getCf_xdr_mc(), "行政处罚", punishModel.getCf_sy(), getIcpCode(punishModel.getCf_xdr_mc()), getDateString(punishModel.getCf_jdrq()), rowIndex, outArrLine);
        });
        return punishList.size() > 0;
    }

    /**
     * @Description 单行写出
     * @Author 梁明辉
     * @Date 15:29 2019-07-05
     * @ModifyDate 15:29 2019-07-05
     * @Params [outSheet, sheet, companyName, type, behavior, creditCode, time]
     * @Return void
     */
    private void writeOneRow(XSSFSheet outSheet, Sheet sheet, String companyName, String type, String behavior, String creditCode, String time, int rowIndex, int[] outArrLine) {
        ExcelUtil.writeDataToXSSF(outSheet, getRowIndex(), 1, companyName);
        ExcelUtil.writeDataToXSSF(outSheet, getRowIndex(), 2, type);
        ExcelUtil.writeDataToXSSF(outSheet, getRowIndex(), 3, behavior);
        ExcelUtil.writeDataToXSSF(outSheet, getRowIndex(), 4, creditCode);
        ExcelUtil.writeDataToXSSF(outSheet, getRowIndex(), 5, time);
        int outLine;
        for (int i = 0; i < outArrLine.length; i++) {
            outLine = outArrLine[i] - 1;
            if (outLine >= 0 && outLine != 1 && outLine != 2 && outLine != 3 && outLine != 4 && outLine != 5) {
                ExcelUtil.writeDataToXSSF(outSheet, getRowIndex(), outLine, ExcelUtil.readHSSFExcelValue(sheet, rowIndex, outLine));
            }
        }
        setRowIndex(getRowIndex() + 1);
    }

    public String getDateString(Date date) {
        String dateStr = (new SimpleDateFormat("yyyy-MM-dd")).format(date);
        return dateStr;
    }

    /**
     * @Description 获取sql
     * @Author 梁明辉
     * @Date 16:27 2019-07-04
     * @ModifyDate 16:27 2019-07-04
     * @Params [companyName]
     * @Return java.lang.String
     */
    private String getConSqlStrByName(String companyName) {
        //query的Condition sql
        String conSqlStr = "";
        if (companyName.contains("(") || companyName.contains("（") || companyName.contains(")") || companyName.contains("）")) {
            String replaceCompanyOne = companyName.replace("(", "（").replace(")", "）");
            String replaceCompanyTwo = companyName.replace("（", "(").replace("）", ")");
            conSqlStr = "in('" + replaceCompanyOne + "','" + replaceCompanyTwo + "')";
        }
        return conSqlStr;
    }

    /**
     * @Description 获取行政处罚
     * @Author 梁明辉
     * @Date 11:46 2019-06-26
     * @ModifyDate 11:46 2019-06-26
     * @Params []
     * @Return java.util.List
     */
    /*public List<AdminPunishModel> getPunishList(String companyName) {

        List<AdminPunishModel> list;
        //只查询 失信主体、失信行为、统一社会信用代码、处罚时间 这里对应处罚相对人名称、处罚事由、统一社会信用代码、处罚决定日期
        QueryModel queryModel = new QueryModel("cf_xdr_mc,cf_sy,cf_jdrq");
        queryModel.addEq("state", Constant.STATE_NORMAL);
        queryModel.addEq("shielded", Constant.STATE_NORMAL);
        queryModel.addCondition(" (cf_gsjzq is null OR cf_gsjzq>=STR_TO_DATE(' " + DateUtil.getNowDate() + "', '%Y-%m-%d'))");
        String conSqlStr = getConSqlStrByName(companyName);
        //如果需要查询中英文括号
        if (StringUtils.isNotBlank(conSqlStr)) {
            queryModel.addCondition(" cf_xdr_mc " + conSqlStr);
        } else {
            queryModel.addEq("cf_xdr_mc", companyName);
        }
        list = MYSQL_BASE_DAO.getList(AdminPunishModel.class, queryModel);
        if (list != null) {
            return list;
        } else {
            return new ArrayList<>();
        }
    }
*/

    /**
     * @Description 获取workbook, 注意此处在前边已经严重是否为excel，此处不必再验证其他格式
     * @Author 梁明辉
     * @Date 14:38 2019-07-04
     * @ModifyDate 14:38 2019-07-04
     * @Params [excelFile]
     * @Return Workbook
     */
    public Workbook getWorkbookByPath(String filePath) {
        File excelFile = new File(filePath);
        try (InputStream inputStream = new FileInputStream(excelFile)) {
            if (excelFile.getName().toLowerCase().endsWith("xls")) {
                return new HSSFWorkbook(inputStream);
            } else {
                return new XSSFWorkbook(inputStream);
            }
        } catch (IOException e) {
            log.info("发现文件IO异常");
            return null;
        }
    }

    /**
     * @Description 验证文件是否存在，是否是excel文件
     * @Author 梁明辉
     * @Date 13:55 2019-07-04
     * @ModifyDate 13:55 2019-07-04
     * @Params [filePath]
     * @Return boolean
     */
    private boolean isEffectiveFile(String filePath) {
        File file = new File(filePath);
        if (!file.exists()) {
            log.info(filePath + ":路径不存在");
            return false;
        }
        String type = filePath.substring(filePath.lastIndexOf(".") == -1 ? (int) file.length() : filePath.lastIndexOf("."));
        if (!".xlsx".equals(type.toLowerCase()) && !".xls".equals(type.toLowerCase())) {
            log.info(filePath + ":不是有效的excel格式");
        }
        return true;
    }

    /**
     * @Description 查询企业基本表
     * @Author 梁明辉
     * @Date 10:54 2019-07-01
     * @ModifyDate 10:54 2019-07-01
     * @Params [companyName]
     * @Return java.lang.String
     */
    private String getIcpCode(String companyName) {
        IcpJibenModel model = icpJibenMapper.getOne(companyName);
        if (model == null) {
            return getPunishCode(companyName);
        }
        //统一社会信用代码不为空
        if (StringUtils.isNotBlank(model.getXinyongdaima())) {
            return model.getXinyongdaima();
        } else if (StringUtils.isNotBlank(model.getZhucehao())) {
            return model.getZhucehao();
        } else {
            return getPunishCode(companyName);
        }

    }

    /**
     * @Description 查处罚表
     * @Author 梁明辉
     * @Date 10:54 2019-07-01
     * @ModifyDate 10:54 2019-07-01
     * @Params []
     * @Return java.lang.String
     */
    private String getPunishCode(String companyName) {
        AdminPunishModel model = punishMapper.getOne(companyName);
        if (model == null) {
            return getLoseCode(companyName);
        }
        //统一社会信用代码不为空
        if (StringUtils.isNotBlank(model.getCf_xdr_shxym())) {
            return model.getCf_xdr_shxym();
        } else if (StringUtils.isNotBlank(model.getCf_xdr_gszc())) {
            //工商注册号
            return model.getCf_xdr_gszc();
        } else if (StringUtils.isNotBlank(model.getCf_xdr_zzjg())) {
            //组织机构代码
            return model.getCf_xdr_zzjg();
        } else if (StringUtils.isNotBlank(model.getCf_xdr_swdj())) {
            //税务登记号
            return model.getCf_xdr_swdj();
        } else if (StringUtils.isNotBlank(model.getCf_xdr_sydw())) {
            //事业单位
            return model.getCf_xdr_sydw();
        } else if (StringUtils.isNotBlank(model.getCf_xdr_shzz())) {
            //社会组织
            return model.getCf_xdr_shzz();
        } else {
            return getLoseCode(companyName);
        }
    }

    /**
     * @Description 查失信
     * @Author 梁明辉
     * @Date 11:02 2019-07-01
     * @ModifyDate 11:02 2019-07-01
     * @Params
     * @Return
     */
    private String getLoseCode(String companyName) {
        YtVerdictLostModel model = lostMapper.getOne(companyName);
        if (model == null) {
            return getUnNormalCode(companyName);
        }
        //统一社会信用代码不为空
        if (StringUtils.isNotBlank(model.getCreditCode())) {
            return model.getCreditCode();
        } else {
            return getUnNormalCode(companyName);
        }

    }

    /**
     * @Description 查经营异常
     * @Author 梁明辉
     * @Date 11:02 2019-07-01
     * @ModifyDate 11:02 2019-07-01
     * @Params
     * @Return
     */
    private String getUnNormalCode(String companyName) {
        YtJingYingYiChangModel model = jinYinMapper.getOne(companyName);
        if (model == null) {
            log.info("companyName未查询到统一社会信用代码" + companyName);
            return "";
        }
        //统一社会信用代码不为空
        if (StringUtils.isNotBlank(model.getCreditCode())) {
            return model.getCreditCode();
        } else {
            log.info("companyName未查询到统一社会信用代码" + companyName);
            return "";
        }
    }

}
