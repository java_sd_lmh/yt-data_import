package com.ld.config.listener;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;

/**
 * @ClassName InItListener
 * @Description 初始化监听
 * @Author 梁明辉
 * @Date 2019/6/28 13:40
 * @ModifyDate 2019/6/28 13:40
 * @Version 1.0
 */
@Component
public class InItListener implements InitializingBean, ServletContextAware {
    @Value("${server.servlet.context-path}")
    private String ctxPath;

    @Override
    public void setServletContext(ServletContext context) {
        //刷新项目配置
        context.setAttribute("ctxPath", ctxPath);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
    }

}
