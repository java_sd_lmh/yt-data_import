package com.ld.controller.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @ClassName IndexController
 * @Author 梁明辉
 * @Description 首页
 * @Date 2019/6/27 11:47
 * @ModifyDate 2019/6/27 11:47
 * @Version 1.0
 */
@Controller
@RequestMapping("/")
@Slf4j
public class IndexController {
    @GetMapping({"/", "/index"})
    public String index() {
        System.out.println(">>>");
        log.info("测试");
        return "common/index/index";
    }
}
