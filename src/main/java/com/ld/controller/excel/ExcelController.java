package com.ld.controller.excel;

import com.alibaba.fastjson.JSONObject;
import com.ld.model.json.JsonResult;
import com.ld.service.excel.ExcelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ClassName ExcelController
 * @Description excel文件补全contro
 * @Author 梁明辉
 * @Date 2019/6/27 16:04
 * @ModifyDate 2019/6/27 16:04
 * @Version 1.0
 */
@Slf4j
@Controller
@EnableAsync
@RequestMapping("/excel")
public class ExcelController {
    @Autowired
    ExcelService excelService;

    @RequestMapping("/index")
    public String index() {
        return "excel/index";
    }

    @PostMapping("/upload")
    @ResponseBody
    public JsonResult upload(@RequestBody JSONObject uploadJson) {
        System.out.println(uploadJson);
        excelService.generateByJson(uploadJson);
        return JsonResult.ok("开始解析传入文件,详细信息请观看后台日志");
    }
}
