package com.ld;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description 烟台
 * @Author 梁明辉
 * @Date 10:45 2019-06-27
 * @ModifyDate 10:45 2019-06-27
 * @Params
 * @Return
 */
@SpringBootApplication
@MapperScan("com.ld.mapper")
public class YtApplication {
    public static void main(String[] args) {
        SpringApplication.run(YtApplication.class, args);
    }
}