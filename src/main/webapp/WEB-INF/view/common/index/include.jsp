<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=utf-8" language="java" %>
<c:set var="ctxPath" value="${ctxPath}"/>
<script>
    const ctxPath = "${ctxPath}";
</script>
<link rel="stylesheet" href="${ctxPath}/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
<link rel="stylesheet" href="${ctxPath}/chartist/css/chartist.min.css">
<link rel="stylesheet" href="${ctxPath}/css/bootstrap.min.css">
<link rel="stylesheet" href="${ctxPath}/css/style.css">
<link rel="stylesheet" href="${ctxPath}/css/responsive.css">

<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.js"></script>
<script src="${ctxPath}/js/jquery.min.js"></script>
<script src="${ctxPath}/chartist/js/chartist.min.js"></script>
<script src="${ctxPath}/js/bootstrap.min.js"></script>
<script src="${ctxPath}/js/jquery.slimscroll.min.js"></script>
<script src="${ctxPath}/js/d3.min.js"></script>
<script src="${ctxPath}/js/topojson.js"></script>
<script src="${ctxPath}/js/fontawesome-all.min.js"></script>
<script src="${ctxPath}/js/jquery-serializeJson.js"></script>