<%@include file="/WEB-INF/view/common/index/include.jsp" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- TITLE OF SITE -->
    <title>DashLoon</title>
    <script src="${ctxPath}/js/datamaps.world.min.js"></script>
    <script src="${ctxPath}/chartist/js/chartist-tooltip-plugin.js"></script>
    <script src="${ctxPath}/chartist/js/chartist-custom.js"></script>
    <script src="${ctxPath}/js/custom.js"></script>
</head>
<body>
<!-- wrapper -->
<div id="wrapper">
    <!-- navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="brand">
            <a href="index.html">
                Dash<span>Loon</span>
            </a>
        </div>
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-fullwidth">
                    <i class="lnr lnr-arrow-left-circle"></i>
                </button>
            </div>
            <form class="navbar-form navbar-left">
                <div class="search-group">
                    <button type="button"><i data-feather="search"></i></button>
                    <input type="text" value="" placeholder="Search..">
                </div>
            </form>
            <div class="navbar-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <i data-feather="bell"></i>
                            <span class="badge badge-bg-1">2</span>
                        </a>
                        <ul class="dropdown-menu notifications">
                            <li><a href="#" class="notification-item"><span class="dot bg-warning"></span>System
                                space
                                is almost full</a></li>
                            <li><a href="#" class="notification-item"><span class="dot bg-danger"></span>You have 9
                                unfinished tasks</a></li>
                            <li><a href="#" class="notification-item"><span class="dot bg-success"></span>Monthly
                                report
                                is available</a></li>
                            <li><a href="#" class="notification-item"><span class="dot bg-warning"></span>Weekly
                                meeting
                                in 1 hour</a></li>
                            <li><a href="#" class="notification-item"><span class="dot bg-success"></span>Your
                                request
                                has been approved</a></li>
                            <li><a href="#" class="more">See all notifications</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <i data-feather="mail"></i>
                            <span class="badge badge-bg-1">3</span>
                        </a>
                        <ul class="dropdown-menu notifications">
                            <li><a href="#" class="notification-item"><span class="dot bg-warning"></span>System
                                space
                                is almost full</a></li>
                            <li><a href="#" class="notification-item"><span class="dot bg-danger"></span>You have 9
                                unfinished tasks</a></li>
                            <li><a href="#" class="notification-item"><span class="dot bg-success"></span>Monthly
                                report
                                is available</a></li>
                            <li><a href="#" class="notification-item"><span class="dot bg-warning"></span>Weekly
                                meeting
                                in 1 hour</a></li>
                            <li><a href="#" class="notification-item"><span class="dot bg-success"></span>Your
                                request
                                has been approved</a></li>
                            <li><a href="#" class="more">See all notifications</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="${ctxPath}/images/parson.png" class="img-circle" alt="parson-img">
                            <i class="icon-submenu fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="#"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
                            <li><a href="#"><i class="lnr lnr-envelope"></i> <span>Message</span></a></li>
                            <li><a href="#"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li>
                            <li><a href="#"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>

    </nav>


    <div id="sidebar-nav" class="sidebar">
        <div class="sidebar-scroll">
            <nav>
                <ul class="nav">

                    <li>
                        <a href="#">
                            <i data-feather="home"></i> <span>Dashboard</span>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <i data-feather="package"></i> <span>components</span>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <i data-feather="file-text"></i> <span>forms</span>
                        </a>
                    </li>
                    <li>
                        <a href="${ctxPath}/excel/index">
                            <i data-feather="file-text"></i> <span>Excel文件补齐</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i data-feather="grid"></i> <span>tables</span>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <i data-feather="map"></i> <span>maps</span>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <i data-feather="bar-chart"></i> <span>charts</span>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <i data-feather="calendar"></i> <span>calendar</span>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <i data-feather="layout"></i> <span>pages</span>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <i data-feather="layout"></i> <span>pages</span>
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <i data-feather="bell"></i> <span>notifications</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>

    </div>

    <div class="tlinks">Collect from <a href="http://www.cssmoban.com/">自助建站</a></div>


    <div class="main">


        <div class="main-content">
            <div class="container-fluid">

                <div class="main-content-head">
                    <h2>hello james!</h2>
                    <p>Welcome to your Dashboard</p>
                </div>

                <div class="profile-content">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="profile-state">
                                <div class="icon-box">
										<span class="icon-bg icon-bg-1">
											<img src="${ctxPath}/images/user-icon.png" alt="user-icon">
										</span>
                                    <h2>total visitors</h2>
                                </div><!--/.icon-box-->
                                <h3>32669</h3>
                                <p>324 new user</p>
                            </div><!--/.profile-state-->
                        </div><!--/.col-->
                        <div class="col-sm-3">
                            <div class="profile-state">
                                <div class="icon-box">
										<span class="icon-bg icon-bg-2">
											<img src="${ctxPath}/images/add-cart.png" alt="add-cart">
										</span>
                                    <h2>total order placeed</h2>
                                </div><!--/.icon-box-->
                                <h3>15500</h3>
                                <p>10 new</p>
                            </div><!--/.profile-state-->
                        </div><!--/.col-->
                        <div class="col-sm-3">
                            <div class="profile-state">
                                <div class="icon-box">
										<span class="icon-bg icon-bg-3">
											<img src="${ctxPath}/images/money-icon.png" alt="money-icon">
										</span>
                                    <h2>total profit</h2>
                                </div><!--/.icon-box-->
                                <h3>$43,900</h3>
                                <p>$340 This Week</p>
                            </div><!--/.profile-state-->
                        </div><!--/.col-->
                        <div class="col-sm-3">
                            <div class="profile-state">
                                <div class="icon-box">
										<span class="icon-bg icon-bg-4">
											<img src="${ctxPath}/images/plane-icon.png" alt="deliver-icon">
										</span>
                                    <h2>delivery peocessing</h2>
                                </div><!--/.icon-box-->
                                <h3>2600</h3>
                                <p>120 This Week</p>
                            </div><!--/.profile-state-->
                        </div><!--/.col-->
                    </div><!--/.row-->

                </div><!--/.profile-content-->

                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="box">
                            <div class="box-head">
                                <h2>statistics</h2>
                                <div class="statistics-data">
                                    <div class="stat-income">
                                        <p><span class="income-dot"></span>income</p>
                                        <p><span class="income-dot outcome-dot"></span>outcome</p>
                                    </div><!--/.stat-income-->
                                </div><!--/.statistics-data-->
                            </div><!--/.box-head-->
                            <div class="item">
                                <div id="statistics_data" class="ct-chart"></div>
                            </div><!--/.item-->
                        </div><!--/.box-->
                    </div><!--/.col-->
                    <div class="col-md-6 col-sm-6">
                        <div class="box">
                            <div class="box-head">
                                <h2>global sales by top locations</h2>
                            </div><!--/.box-head-->
                            <div class="item">
                                <div class="map" id="bubbles" style="position:relative;height: 170px;">

                                </div>
                                <div class="statistics-data">
                                    <div class="map-area">
                                        <ul>
                                            <li>
                                                <span class="map-dot ps-dot2"></span>north america
                                            </li>
                                            <li>
                                                <span class="map-dot ps-dot3"></span>south america
                                            </li>
                                            <li>
                                                <span class="map-dot ps-dot4"></span>asia
                                            </li>
                                            <li>
                                                <span class="map-dot ps-dot1"></span>africa
                                            </li>
                                        </ul>
                                    </div><!--/.map-area-->
                                </div><!--/.statistics-data-->
                            </div><!--/.item-->
                        </div><!--/.box-->
                    </div><!--/.col-->

                </div><!--/.row-->

                <div class="row">
                    <div class="col-sm-8">
                        <div class="box">
                            <div class="box-head">
                                <h2>top 5 search item</h2>
                            </div><!--/.box-head-->
                            <div class="item">
                                <div class="item-responsive ">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>ID#</th>
                                            <th>Product Name</th>
                                            <th>Dealer</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead><!--/thead-->
                                        <tbody>
                                        <tr>
                                            <td><a href="#">1</a></td>
                                            <td>Samsung Galaxy S8</td>
                                            <td>Shamsu</td>
                                            <td>$25050</td>
                                            <td>185</td>
                                            <td>
                                                <span class="item-status delivered">Delivered</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><a href="#">2</a></td>
                                            <td>Samsung Galaxy S8</td>
                                            <td>Shamsu</td>
                                            <td>$25050</td>
                                            <td>185</td>
                                            <td><span class="item-status cancel">Cancel</span></td>
                                        </tr>
                                        <tr>
                                            <td><a href="#">3</a></td>
                                            <td>Samsung Galaxy S8</td>
                                            <td>Shamsu</td>
                                            <td>$25050</td>
                                            <td>185</td>
                                            <td><span class="item-status delivered">Delivered</span></td>
                                        </tr>
                                        <tr>
                                            <td><a href="#">4</a></td>
                                            <td>Samsung Galaxy S8</td>
                                            <td>Shamsu</td>
                                            <td>$25050</td>
                                            <td>185</td>
                                            <td><span class="item-status cancel">Cencel</span></td>
                                        </tr>
                                        <tr>
                                            <td><a href="#">5</a></td>
                                            <td>Samsung Galaxy S8</td>
                                            <td>Shamsu</td>
                                            <td>$25050</td>
                                            <td>185</td>
                                            <td><span class="item-status delivered">Delivered</span></td>
                                        </tr>
                                        </tbody><!--/tbody-->
                                    </table><!--/.table-->
                                </div><!--/.table-responsive-->
                            </div><!--/.item-->
                        </div><!--/.box-->
                    </div><!--/.col-->
                    <div class="col-sm-4">
                        <div class="box">
                            <div class="box-head">
                                <h2>product status</h2>
                            </div><!--/.box-head-->
                            <div class="item">
                                <div id="product_status" class="ct-chart"></div>
                                <div class="statistics-data">
                                    <div class="stat-income pl0">
                                        <p><span class="income-dot ps-dot2"></span>margin</p>
                                        <p class="ml10"><span class="income-dot ps-dot3"></span>profit</p>
                                        <p class="ml10"><span class="income-dot ps-dot4"></span>lost</p>
                                        <p><span class="income-dot ps-dot1"></span>others</p>
                                    </div><!--/.stat-income-->
                                </div><!--/.statistics-data-->
                            </div><!--/.item-->
                        </div><!--/.box-->
                    </div><!--/.col-->

                </div><!--/.row-->

                <div class="row">
                    <div class="col-sm-12">
                        <div class="box">
                            <div class="box-head">
                                <h2>yearly expense</h2>
                                <div class="year-info">
                                    <div class="year-info-content">
                                        <div class="year-color year-color-1"></div>
                                        <h4>2015</h4>
                                    </div><!--/.year-info-content-->
                                    <div class="year-info-content">
                                        <div class="year-color year-color-2"></div>
                                        <h4>2016</h4>
                                    </div><!--/.year-info-content-->
                                    <div class="year-info-content">
                                        <div class="year-color year-color-3"></div>
                                        <h4>2017</h4>
                                    </div><!--/.year-info-content-->
                                </div><!--/.yaer-info-->
                            </div><!--/.box-head-->
                            <div id="yarly_expence" class="ct-chart"></div>
                        </div><!--/.box-->

                    </div><!--/.col-->

                </div><!--/.row-->

            </div><!--/.container-fluid-->;

        </div><!--/.main-content-->

    </div>
    <!-- END MAIN -->

    <div class="clearfix"></div>

    <footer>
        <div class="container-fluid">
            <p>&copy; 2017 ThemeSINE. All Right Reserved - More Templates <a href="http://www.cssmoban.com/"
                                                                             target="_blank" title="模板之家">模板之家</a> -
                Collect from <a href="http://www.cssmoban.com/" title="网页模板" target="_blank">网页模板</a></p>
        </div>

    </footer>

</div>
<!-- END WRAPPER -->
</body>
</html>