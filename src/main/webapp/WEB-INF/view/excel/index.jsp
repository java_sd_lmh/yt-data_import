<%@include file="/WEB-INF/view/common/index/include.jsp" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- TITLE OF SITE -->
    <title>Excel文件数据补全</title>
    <script type="text/javascript">
        function doGenerate() {
            if ($("#filePath").val() == "") {
                alert("请输入文件路径");
                return;
            }
            $.ajax({
                type: "POST",
                url: ctxPath + "/excel/upload",
                contentType: 'application/json;charset=utf-8',
                data: JSON.stringify($("#generateForm").serializeJson()),
                dataType: "json",
                success: function (data) {
                    alert(data.data);
                    location.reload();
                }
            });
        }
    </script>
</head>
<body>
<!-- wrapper -->
<div id="wrapper">
    <div id="sidebar-nav" class="sidebar">
        <div class="sidebar-scroll">
            <nav>
                <ul class="nav">
                    <li>
                        <a href="#">
                            <i data-feather="home"></i> <span>Dashboard</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="main">
        <div class="main-content">
            <div class="container-fluid">

                <div class="main-content-head">
                    <h2>hello james!</h2>
                    <p>Welcome to your Dashboard</p>
                </div>
                <!-- 导入文件行 -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="box">
                            <form method="post" enctype="multipart/form-data" autocomplete="off"
                                  id="generateForm">
                                <div class="item">
                                    <div class="item-responsive ">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>文件路径</th>
                                                <th>企业名/企业名所在列</th>
                                                <th>需要导出列数(用,隔开)</th>
                                                <th>是否导出失信被执行人</th>
                                                <th>是否导出经营异常</th>
                                                <th>是否导出行政处罚</th>
                                                <th>操作</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td width="30%"><input type="text" name="filePath" id="filePath"
                                                                       style="width: 400px"/></td>
                                                <td><input type="number" name="companyLine" placeholder="默认为第2行"/></td>
                                                <td><input type="text" name="queryArr" placeholder="默认只导出为企业名所在行"
                                                           style="width: 200px"/></td>
                                                <td><input type="checkbox" name="needLost" checked="checked"></td>
                                                <td><input type="checkbox" name="needJinYin" checked="checked">
                                                </td>
                                                <td><input type="checkbox" name="needPunish" checked="checked">
                                                </td>
                                                <td>
                                                <span class="item-status delivered"
                                                      onclick="doGenerate();">点击开始执行</span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- 列表行 -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="box">
                            <div class="box-head">
                                <h2>top 5 search item</h2>
                            </div><!--/.box-head-->
                            <div class="item">
                                <div class="item-responsive ">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>ID#</th>
                                            <th>Product Name</th>
                                            <th>Dealer</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead><!--/thead-->
                                        <tbody>
                                        <tr>
                                            <td><a href="#">1</a></td>
                                            <td>Samsung Galaxy S8</td>
                                            <td>Shamsu</td>
                                            <td>$25050</td>
                                            <td>185</td>
                                            <td>
                                                <span class="item-status delivered">Delivered</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><a href="#">2</a></td>
                                            <td>Samsung Galaxy S8</td>
                                            <td>Shamsu</td>
                                            <td>$25050</td>
                                            <td>185</td>
                                            <td><span class="item-status cancel">Cancel</span></td>
                                        </tr>
                                        <tr>
                                            <td><a href="#">3</a></td>
                                            <td>Samsung Galaxy S8</td>
                                            <td>Shamsu</td>
                                            <td>$25050</td>
                                            <td>185</td>
                                            <td><span class="item-status delivered">Delivered</span></td>
                                        </tr>
                                        <tr>
                                            <td><a href="#">4</a></td>
                                            <td>Samsung Galaxy S8</td>
                                            <td>Shamsu</td>
                                            <td>$25050</td>
                                            <td>185</td>
                                            <td><span class="item-status cancel">Cencel</span></td>
                                        </tr>
                                        <tr>
                                            <td><a href="#">5</a></td>
                                            <td>Samsung Galaxy S8</td>
                                            <td>Shamsu</td>
                                            <td>$25050</td>
                                            <td>185</td>
                                            <td><span class="item-status delivered">Delivered</span></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
</body>
</html>
<body>
